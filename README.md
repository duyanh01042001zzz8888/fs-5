# Docker Swarm and Traefik deploy

- Database và Traefik chạy trên leader node
- Các kết nối đã được cấu hình sẵn dựa trên IP của leader và worker node

## 1. Cài đặt trên leader node

- Có thể build Dockerfile thành image và đưa lên Container Registry để tiện cho việc triển khai

```bash
git clone https://gitlab.com/duyanh01042001zzz8888/fs-2.git
cd fs-2
```

Chạy MySQL

```bash
cd docker/mysql
docker build -t vnec_mysql .
docker run --name vnec_mysql -p 3306:3306 -d vnec_mysql
```

Chạy Traefik

```bash
cd ../../..
git clone https://gitlab.com/duyanh01042001zzz8888/fs-5.git
cd fs-5
```

```bash
docker compose -p vnec_traefik up --build
```


## 2. Khởi tạo Docker Swarm trên leader node

```bash
docker swarm init --advertise-addr=[ip]
```

IP có thể là public IP nếu muốn nhận kết nối từ các worker node ngoài mạng, hoặc a private IP với các worker node trong mạng.

![Docker Swarm Init](https://i.imgur.com/sjuNLl8.png)

## 3. Kết nối các worker node tới leader node

Sử dụng lệnh được sinh ra khi khởi tạo leader node để kết nối các worker node với leader node.

```bash
 docker swarm join --token [token] [ip]:[port]
```

## 4. Triển khai API image từ leader node tới các worker node

Rebuild lại API image và đưa lên GitLab Container Registry

```bash
docker login https://registry.gitlab.com
docker build -t registry.gitlab.com/duyanh01042001zzz8888/fs-2 .
docker push registry.gitlab.com/duyanh01042001zzz8888/fs-2
```

Tạo service trên leader node

```bash
docker service create --name vnec_api --replicas 2 -p 3000:3000 registry.gitlab.com/duyanh01042001zzz8888/fs-2
```

Xem các service đang được chạy

```bash
docker service ls
```

Xem các task/container được được chạy trên service

```bash
docker service ps [service id]
```
